package com.provajava2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvaJava2Application {

    public static void main(String[] args) {
        SpringApplication.run(ProvaJava2Application.class, args);
    }
}
