package com.provajava2.controller;


import com.provajava2.models.GruposModel;
import com.provajava2.repository.GrupoRepository;
import com.provajava2.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class IndexController {

    @Autowired
    private GrupoRepository gr;

    @Autowired
    private ProdutoRepository pr;

    @GetMapping("/listar")
    public String home() {
        return "index";
    }

    @GetMapping("/formulario/cadastro")
    public String cadastro(Model model, GruposModel gruposModel) {
        model.addAttribute("produtos", pr.findAll());
        return "formulario";
    }

    @PostMapping("/novo/grupo")
    public String index(GruposModel gruposModel) {
        gr.save(gruposModel);
        return  "redirect:/listar";
    }
}
