package com.provajava2.models;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class GruposModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String nomeGrupo;

    @ManyToMany
    private List<ProdutosModel> listaDeProdutos = new ArrayList<>();

}
