package com.provajava2.repository;

import com.provajava2.models.GruposModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GrupoRepository extends JpaRepository<GruposModel, Long> {
}
