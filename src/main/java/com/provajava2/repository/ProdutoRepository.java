package com.provajava2.repository;

import com.provajava2.models.ProdutosModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProdutoRepository extends JpaRepository<ProdutosModel, Long> {
}
