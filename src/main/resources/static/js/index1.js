$(function() {
    var produtosDisponiveis = $('#produtosDisponiveis');
    var produtosSelecionados = $('#produtosSelecionados');
    function moveItems(origem, destino) {
        $(origem).find(':selected').appendTo(destino);
    }
    $('#btnLeft').click(function() {
        moveItems(produtosSelecionados, produtosDisponiveis);
    });
    $('#btnRigth').click(function() {
        moveItems(produtosDisponiveis, produtosSelecionados);
    });

});